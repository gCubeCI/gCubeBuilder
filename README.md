# gCubeRelease

Jenkins Pipeline script to manage a complete gCube release.

## Requirements
* [Jenkins](https://jenkins.io/) ver. 2.164.2 or newer
* [Pipeline plugin](https://wiki.jenkins.io/display/JENKINS/Pipeline+Plugin)
* [Pipeline: Maven](https://plugins.jenkins.io/pipeline-maven)
* [Pipeline: Basic Steps](https://plugins.jenkins.io/workflow-basic-steps) 
* [Kubernetes](https://plugins.jenkins.io/kubernetes) (for the YAML parser)
* [NodeLabelParameter](https://plugins.jenkins.io/nodelabelparameter) (to allow to dynamically select the node on which a job should be executed)
* [Email Extension](https://plugins.jenkins.io/email-ext) (to send emails with attachments)
* Jenkins configured with a JDK global tool named 'OpenJDK 8'
* Jenkins configured with a Maven global tool named 'Maven 3.6.2'
* One or more Jenkins agents labeled as 'CD'

## Expected parameters
Parameters from the Jenkins pipeline project:
* Type: a choice of 'SNAPSHOT-DRY-RUN', 'SNAPSHOT', 'RELEASE-DRY-RUN', 'STAGING', 'RELEASE'
* gCube_release_version: the gCube version to build (must match the YAML file in /releases)
* cleanup_gcube_artifacts: a boolean flag to decide whether to wipe out the gCube artifacts from the local repository before the build
* cleanup_local_repo: a boolean flag to decide whether to wipe out all the artifacts from the local repository before the build
* resume_from: a build number. If specified, the new build will resume from the last failed job of that build

## References 
* [Jenkins Pipeline](https://jenkins.io/doc/book/pipeline/) 
* [Pipeline Syntax](https://jenkins.io/doc/book/pipeline/syntax/)

## Wiki doc

* [CI: Release Pipeline](https://wiki.gcube-system.org/gcube/Continuous_Integration:_Releases_Jenkins_Pipeline)


## License
This project is licensed under the [EUPL V.1.1 License](LICENSE.md).